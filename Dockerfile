FROM alpine:latest
LABEL maintainer="shane@northernv.com"

# Download and install hugo
ARG VERSION
ENV HUGO_BINARY hugo_${VERSION}_linux-64bit

# Download and Install hugo
RUN mkdir /usr/local/hugo
ADD https://github.com/gohugoio/hugo/releases/download/v${VERSION}/${HUGO_BINARY}.tar.gz /usr/local/hugo/
RUN tar xzf /usr/local/hugo/${HUGO_BINARY}.tar.gz -C /usr/local/hugo/ \
	&& ln -s /usr/local/hugo/hugo /usr/local/bin/hugo \
	&& rm /usr/local/hugo/${HUGO_BINARY}.tar.gz

# Create working directory
RUN mkdir /src
WORKDIR /src

# Default hugo server port
EXPOSE 1313

# By default, serve site
CMD ["hugo"]
